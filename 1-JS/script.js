"use strict";

function getPassword(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
let password = getPassword(0, 101);
console.log(password);

let tries = 5;

for (let i = 0; i < tries; i++) {
  let passwordTry = +prompt(`Introduzca un numero entre 0 y 100`);
  if (passwordTry === password) {
    alert(`Contraseña correcta!`);
    break;
  } else if (passwordTry < password) {
    alert(`El numero de la contraseña es MAYOR`);
  } else if (passwordTry > password) {
    alert(`El numero de la contraseña es MENOR`);
  }
  if (i === tries - 1) {
    alert(`Perdiste! El numero era ${password}`);
  }
}
