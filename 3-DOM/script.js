"use strict";

const formatNum = (num) => {
  return num < 10 ? "0" + num : num;
};

const body = document.querySelector("body");
let div = document.createElement("div");
body.appendChild(div);

setInterval(() => {
  let date = new Date();
  let currentHour = `${formatNum(date.getHours())} : ${formatNum(
    date.getMinutes()
  )} : ${formatNum(date.getSeconds())}`;
  div.innerHTML = currentHour;
}, 1000);
